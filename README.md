# `auto_from`

This library exports a derive macro for `From`. It is used to automatically
generate `std::convert::From` implementations for enum variants containing
only one field. It currently works for tuple or struct variants. Here is a
small example:

```rust
use auto_from::From;

#[auto_from]
#[derive(Debug, PartialEq, From)]
enum Foo {
    Int(i32),
    Long { value: i64 },
}

fn main() {
    assert_eq!(Foo::Int(24), Foo::from(24i32));
    assert_eq!(Foo::Long{ value: 24 }, Foo::from(24i64));
}
```

This should be most useful when constructing error enums that require
writing many `From` implementations, or using many
`.map_error(|e| MyError::Variant(e))` calls across the code. This crate
just simplifies the process.
